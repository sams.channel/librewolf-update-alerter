function ulw_libreWolf() {
    let uaVersion = /(\d{3,4}\.\d)/g;
    let longVersion = /(\d{3,4}\.\d\.*\d*\-*\d*)/g;
    let currVersion = navigator.userAgent.match(uaVersion);

    let osPattn = /(Linux)|(Windows)|(Mac\s*OS)/ig;
    let os = navigator.userAgent.match(osPattn)[0];
    let updateUrl = "https://librewolf.net/installation/" + os + "/";
    updateUrl = updateUrl.replace(" ","").toLowerCase();

    if (window.location.href != updateUrl) {
        window.location.href = updateUrl;
    }

    if (window.location.href == updateUrl) {
        let releaseLink = document.getElementsByClassName("button")[0].textContent;
        let latestVersion = releaseLink.match(longVersion);
        let latestShortVersion = releaseLink.match(uaVersion);

        if (parseFloat(currVersion) > parseFloat(latestShortVersion)) {
            return;
        }

        browser.storage.local.get("updatedVersion").then(function(item) {
            if (item == null) return;
            let versions = [ item.updatedVersion, latestVersion[0] ];
            window.postMessage({type: 'lwLatestVersion', data: versions}, '*');
        });

        let versions = [ currVersion[1], latestVersion[0] ];
        if (currVersion[1] != latestVersion[0]) {
            window.postMessage({type: 'lwLatestVersion', data: versions}, '*');
        }
    }
}

setInterval(ulw_libreWolf, 1000);
