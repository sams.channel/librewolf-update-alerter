### LibreWolf Update Alerter
> LibreWolf (Firefox) extension to check if an update is available for LibreWolf.

To use, simply make [https://librewolf.net](https://librewolf.net), one of your homepage tabs and optionally enable the button
which will report the current version of LibreWolf that you are using.

On Windows/Mac OS X, when an update is available, you will be directed
to the GitLab releases page for executables for your operating system.

On Linux, you will be similary directed to the GitLab releases page for AppImages.

