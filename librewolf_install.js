function setItem() {}

function onError(error) {
    window.alert(error);
}

browser.runtime.onInstalled.addListener(function(details) {
    if (details.reason === "install") {
        let os = { os: "(Detect)" };
        let arch = { arch: "(Detect)" };
        let pkg = { pkg: "(Automatic)" };
        browser.storage.local.set(os).then(setItem, onError);
        browser.storage.local.set(arch).then(setItem, onError);
        browser.storage.local.set(pkg).then(setItem, onError);
    }
});
