function ulw_getVersion() {
    let uaVersion = /(\d{3,4}\.\d)/g;
    return navigator.userAgent.match(uaVersion);
}

function ulw_getOS() {
    let osPattn = /(Linux)|(Windows)|(Mac\s*OS)/ig;
    return navigator.userAgent.match(osPattn)[0];
}

function ulw_getArchitecture() {
    let archPattn = /(i686)|(Intel)|(ARM)|(x86_64)|(x64)|(amd64)|(arm64)|(aarch64)/ig;
    let architecture = navigator.userAgent.match(archPattn)[0].toLowerCase();

    // Linux, Windows (and Mac on Intel) (x86-64).
    if (architecture == "x64" || architecture == "amd64" || architecture == "intel") {
        architecture = "x86_64";
    }

    // Windows 32-bit (x86).
    // This will only work if the userAgent does not report that
    // it is running on a Windows 64-bit system even if it is really 32-bit.
    if (architecture != "x86_64" && architecture != "arm64" && navigator.platform == "Win32") {
        architecture = "i686";
    }
    // Mac OS X on Apple Silicon (ARM/aarch64).
    else if (architecture == "arm") {
        architecture = "aarch64";
    }

    return architecture;
}

function ulw_update(currVersion, latestVersion, os, architecture) {
    if (currVersion != null && latestVersion != null
    && os != null && architecture != null) {
        let lcOs = os.replace(" ", "").toLowerCase();
        let updateUrl = "https://gitlab.com/librewolf-community/browser/appimage/-/releases";
        if (lcOs == "windows") {
            updateUrl = updateUrl.replace("appimage", "bsys6");
        }
        else if (lcOs == "macos") {
            updateUrl = updateUrl.replace("appimage", lcOs);
        }

        window.location.href = browser.runtime.getURL("librewolf_update.html"
        + "?currVersion=" + encodeURIComponent(currVersion)
        + "&latestVersion=" + encodeURIComponent(latestVersion)
        + "&os=" + encodeURIComponent(os)
        + "&architecture=" + encodeURIComponent(architecture)
        + "&updateLink=" + updateUrl);
    }
}

document.addEventListener('DOMContentLoaded', function() {
    let currVersion = ulw_getVersion();
    let cv = document.getElementById("cv");
    cv.innerText = "Current version: " + currVersion[1];
    browser.storage.local.get("currLongVersion").then(function(item) {
        if (item == null) return;
        cv.innerText = "Current version: " + item.currLongVersion;
    });
});

window.addEventListener('message', function(event) {
    if (event.source === window && event.data.type === 'lwLatestVersion') {
        let currVersion = event.data.data[0];
        let os = ulw_getOS();
        let architecture = ulw_getArchitecture();
        let latestVersion = event.data.data[1];
        ulw_update(currVersion, latestVersion, os, architecture);
    }
});
