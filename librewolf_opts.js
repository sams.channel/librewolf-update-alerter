function setItem() {}

function gotOS(item) {
    document.getElementById("os").value = item.os;
}

function gotArch(item) {
    document.getElementById("arch").value = item.arch;
}

function gotPkg(item) {
    document.getElementById("pkg").value = item.pkg;
}

function onError(error) {
    window.alert(error);
}

document.addEventListener('DOMContentLoaded', function() {
    browser.storage.local.get('os').then(gotOS, onError);
    browser.storage.local.get('arch').then(gotArch, onError);
    browser.storage.local.get('pkg').then(gotPkg, onError);
});

const osElement = document.getElementById("os");
osElement.addEventListener("change", function(event) {
    let os = { os: event.target.value };
    browser.storage.local.set(os).then(setItem, onError);
});

const archElement = document.getElementById("arch");
archElement.addEventListener("change", function(event) {
    let arch = { arch: event.target.value };
    browser.storage.local.set(arch).then(setItem, onError);
});

const pkgElement = document.getElementById("pkg");
pkgElement.addEventListener("change", function(event) {
    let pkg = { pkg: event.target.value };
    browser.storage.local.set(pkg).then(setItem, onError);
});
