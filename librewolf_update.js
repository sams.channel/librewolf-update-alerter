let g_update_currVersion;
let g_latestVersion;
let g_os;

function gotOS(item) {
    if (item == null || item.os == "(Detect)") return;
    g_os = item.os;
}

function gotArch(item) {
    if (item == null || item.arch == "(Detect)") return;
    let osArch = " (" + g_os + " " + item.arch + ")";
    document.getElementById("lv").innerText = "Latest version: " + g_latestVersion + osArch;
}

function gotPkg(item) {
    let updateUrl = "https://gitlab.com/librewolf-community/browser/appimage/-/releases";
    if (item == null || item.pkg == "(Automatic)") return;
    if (item.pkg == "Manual package or executable") {
        updateUrl = updateUrl.replace("appimage", "bsys6");
        if (g_os == "Mac OS") {
            updateUrl = updateUrl.replace("bsys6", "macos");
        }
    }
    if (item.pkg == "Package manager") {
        document.getElementById("update").style.visibility = "hidden";
        if (g_os == "Windows") {
            document.getElementById("choco").classList.remove("pm_win");
            document.getElementById("winget").classList.remove("pm_win");
            document.getElementById("scoop1").classList.remove("pm_win");
            document.getElementById("scoop2").classList.remove("pm_win");
        }
        else if (g_os == "Mac OS") {
            document.getElementById("brew").classList.remove("pm_mac");
        }
        else if (g_os == "Linux") {
            document.getElementById("paru").classList.remove("pm_linux");
            document.getElementById("apt").classList.remove("pm_linux");
            document.getElementById("dnf").classList.remove("pm_linux");
        }
    }
    document.getElementById("update").href = updateUrl;
}

function setUpdatedItem() {
    console.info("Stored updated version.");
}

function setCurrItem() {
    console.info("Stored current version.");
}

function onError(error) {
    console.error(error);
}

document.addEventListener('DOMContentLoaded', function() {
    let urlParams = new URLSearchParams(window.location.search);
    let currVersion = urlParams.get('currVersion');
    g_latestVersion = urlParams.get('latestVersion');
    g_os = urlParams.get('os');
    let architecture = urlParams.get('architecture');
    let updateLink = urlParams.get('updateLink');

    let osArch = " (" + g_os + " " + architecture + ")";

    let updatedVersion = { updatedVersion: g_latestVersion };
    browser.storage.local.set(updatedVersion).then(setUpdatedItem, onError);

    document.getElementById("cv").innerText = "Current version: " + currVersion;
    document.getElementById("lv").innerText = "Latest version: " + g_latestVersion + osArch;
    document.getElementById("update").href = updateLink;

    if (currVersion === g_latestVersion) {
        document.title = "LibreWolf Up to Date";
        document.getElementById("title").innerText = "LibreWolf Up to Date";
        document.getElementById("update").style.visibility = "hidden";
        document.getElementById("latest").classList.remove("latest");
        let currLongVersion = { currLongVersion: g_latestVersion };
        browser.storage.local.set(currLongVersion).then(setCurrItem, onError);
    }

    browser.storage.local.get('os').then(gotOS, onError);
    browser.storage.local.get('arch').then(gotArch, onError);
    browser.storage.local.get('pkg').then(gotPkg, onError);
});
